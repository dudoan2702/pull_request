create table ABSTRACT_PRODUCTS (
    ID bigint not null auto_increment,
    CREATED_AT datetime,
    CREATED_BY bigint,
    UPDATED_AT datetime,
    UPDATED_BY bigint,
    DELETED boolean not null,
    CATEGORY_ID bigint not null,
    CLIENT_ID bigint not null,
    DESC varchar(200) not null,
    NAME varchar(100) not null,
    constraint ABSTRACT_PRODUCTS_PK primary key(ID)
);

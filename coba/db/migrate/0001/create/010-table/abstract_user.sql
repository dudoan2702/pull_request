create table ABSTRACT_USER (
    ID bigint not null auto_increment,
    CREATED_AT datetime,
    CREATED_BY bigint,
    UPDATED_AT datetime,
    UPDATED_BY bigint,
    DELETED boolean not null,
    CLIENT_ID bigint not null,
    STATUS binary(2) not null,
    PASSWORD varchar(100) not null,
    LOGIN_ID varchar(30) not null,
    DISPLAY_NAME varchar(100) not null,
    constraint ABSTRACT_USER_PK primary key(ID)
);

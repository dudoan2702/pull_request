create table ABSTRACT_TAGS (
    ID bigint not null auto_increment,
    CREATED_AT datetime,
    CREATED_BY bigint,
    UPDATED_AT datetime,
    UPDATED_BY bigint,
    DELETED boolean not null,
    CLIENT_ID bigint not null,
    NAME binary(100) not null,
    constraint ABSTRACT_TAGS_PK primary key(ID)
);

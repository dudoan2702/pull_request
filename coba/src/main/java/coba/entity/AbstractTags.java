package coba.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;

@MappedSuperclass
public class AbstractTags extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    /** idプロパティ */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(precision = 19, nullable = false, unique = true)
    public Long id;

    /** nameプロパティ */
    @Column(length = 100, nullable = false, unique = false)
    public byte[] name;

    /** clientIdプロパティ */
    @Column(precision = 19, nullable = false, unique = false)
    public Long clientId;

    /** tagMapList関連プロパティ */
    @OneToMany(mappedBy = "tag")
    public List<AbstractTagMap> tagMapList;
}
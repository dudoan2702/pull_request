package coba.entity;

import javax.annotation.Generated;

import coba.entity.CategoryNames._CategoryNames;
import coba.entity.ClientNames._ClientNames;
import coba.entity.ProductsNames._ProductsNames;
import coba.entity.TagMapNames._TagMapNames;
import coba.entity.TagsNames._TagsNames;
import coba.entity.UserNames._UserNames;

/**
 * 名前クラスの集約です。
 *
 */
@Generated(value = {"S2JDBC-Gen 2.4.45", "org.seasar.extension.jdbc.gen.internal.model.NamesAggregateModelFactoryImpl"}, date = "Aug 26, 2013 4:41:07 PM")
public class Names {

    /**
     * {@link AbstractCategory}の名前クラスを返します。
     *
     * @return Categoryの名前クラス
     */
    public static _CategoryNames category() {
        return new _CategoryNames();
    }

    /**
     * {@link AbstractClient}の名前クラスを返します。
     *
     * @return Clientの名前クラス
     */
    public static _ClientNames client() {
        return new _ClientNames();
    }

    /**
     * {@link AbstractProducts}の名前クラスを返します。
     *
     * @return Productsの名前クラス
     */
    public static _ProductsNames products() {
        return new _ProductsNames();
    }

    /**
     * {@link AbstractTagMap}の名前クラスを返します。
     *
     * @return TagMapの名前クラス
     */
    public static _TagMapNames tagMap() {
        return new _TagMapNames();
    }

    /**
     * {@link AbstractTags}の名前クラスを返します。
     *
     * @return Tagsの名前クラス
     */
    public static _TagsNames tags() {
        return new _TagsNames();
    }

    /**
     * {@link AbstractUser}の名前クラスを返します。
     *
     * @return Userの名前クラス
     */
    public static _UserNames user() {
        return new _UserNames();
    }
}
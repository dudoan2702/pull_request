package coba.entity;

import coba.entity.ProductsNames._ProductsNames;

import javax.annotation.Generated;
import org.seasar.extension.jdbc.name.PropertyName;

/**
 * {@link AbstractCategory}のプロパティ名の集合です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.45", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "Aug 26, 2013 4:41:07 PM")
public class CategoryNames {

    /**
     * idのプロパティ名を返します。
     * 
     * @return idのプロパティ名
     */
    public static PropertyName<Long> id() {
        return new PropertyName<Long>("id");
    }

    /**
     * nameのプロパティ名を返します。
     * 
     * @return nameのプロパティ名
     */
    public static PropertyName<String> name() {
        return new PropertyName<String>("name");
    }

    /**
     * descのプロパティ名を返します。
     * 
     * @return descのプロパティ名
     */
    public static PropertyName<String> desc() {
        return new PropertyName<String>("desc");
    }

    /**
     * clientIdのプロパティ名を返します。
     * 
     * @return clientIdのプロパティ名
     */
    public static PropertyName<Long> clientId() {
        return new PropertyName<Long>("clientId");
    }

    /**
     * productsListのプロパティ名を返します。
     * 
     * @return productsListのプロパティ名
     */
    public static _ProductsNames productsList() {
        return new _ProductsNames("productsList");
    }

    /**
     * @author S2JDBC-Gen
     */
    public static class _CategoryNames extends PropertyName<AbstractCategory> {

        /**
         * インスタンスを構築します。
         */
        public _CategoryNames() {
        }

        /**
         * インスタンスを構築します。
         * 
         * @param name
         *            名前
         */
        public _CategoryNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         * 
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _CategoryNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * idのプロパティ名を返します。
         *
         * @return idのプロパティ名
         */
        public PropertyName<Long> id() {
            return new PropertyName<Long>(this, "id");
        }

        /**
         * nameのプロパティ名を返します。
         *
         * @return nameのプロパティ名
         */
        public PropertyName<String> name() {
            return new PropertyName<String>(this, "name");
        }

        /**
         * descのプロパティ名を返します。
         *
         * @return descのプロパティ名
         */
        public PropertyName<String> desc() {
            return new PropertyName<String>(this, "desc");
        }

        /**
         * clientIdのプロパティ名を返します。
         *
         * @return clientIdのプロパティ名
         */
        public PropertyName<Long> clientId() {
            return new PropertyName<Long>(this, "clientId");
        }

        /**
         * productsListのプロパティ名を返します。
         * 
         * @return productsListのプロパティ名
         */
        public _ProductsNames productsList() {
            return new _ProductsNames(this, "productsList");
        }
    }
}
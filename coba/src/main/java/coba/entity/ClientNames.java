package coba.entity;

import coba.entity.UserNames._UserNames;

import java.sql.Timestamp;
import javax.annotation.Generated;
import org.seasar.extension.jdbc.name.PropertyName;

/**
 * {@link AbstractClient}のプロパティ名の集合です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.45", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "Aug 26, 2013 4:41:07 PM")
public class ClientNames {

    /**
     * idのプロパティ名を返します。
     * 
     * @return idのプロパティ名
     */
    public static PropertyName<Long> id() {
        return new PropertyName<Long>("id");
    }

    /**
     * firstNameのプロパティ名を返します。
     * 
     * @return firstNameのプロパティ名
     */
    public static PropertyName<String> firstName() {
        return new PropertyName<String>("firstName");
    }

    /**
     * lastNameのプロパティ名を返します。
     * 
     * @return lastNameのプロパティ名
     */
    public static PropertyName<String> lastName() {
        return new PropertyName<String>("lastName");
    }

    /**
     * birthDayのプロパティ名を返します。
     * 
     * @return birthDayのプロパティ名
     */
    public static PropertyName<Timestamp> birthDay() {
        return new PropertyName<Timestamp>("birthDay");
    }

    /**
     * dirのプロパティ名を返します。
     * 
     * @return dirのプロパティ名
     */
    public static PropertyName<String> dir() {
        return new PropertyName<String>("dir");
    }

    /**
     * templateのプロパティ名を返します。
     * 
     * @return templateのプロパティ名
     */
    public static PropertyName<String> template() {
        return new PropertyName<String>("template");
    }

    /**
     * userListのプロパティ名を返します。
     * 
     * @return userListのプロパティ名
     */
    public static _UserNames userList() {
        return new _UserNames("userList");
    }

    /**
     * @author S2JDBC-Gen
     */
    public static class _ClientNames extends PropertyName<AbstractClient> {

        /**
         * インスタンスを構築します。
         */
        public _ClientNames() {
        }

        /**
         * インスタンスを構築します。
         * 
         * @param name
         *            名前
         */
        public _ClientNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         * 
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _ClientNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * idのプロパティ名を返します。
         *
         * @return idのプロパティ名
         */
        public PropertyName<Long> id() {
            return new PropertyName<Long>(this, "id");
        }

        /**
         * firstNameのプロパティ名を返します。
         *
         * @return firstNameのプロパティ名
         */
        public PropertyName<String> firstName() {
            return new PropertyName<String>(this, "firstName");
        }

        /**
         * lastNameのプロパティ名を返します。
         *
         * @return lastNameのプロパティ名
         */
        public PropertyName<String> lastName() {
            return new PropertyName<String>(this, "lastName");
        }

        /**
         * birthDayのプロパティ名を返します。
         *
         * @return birthDayのプロパティ名
         */
        public PropertyName<Timestamp> birthDay() {
            return new PropertyName<Timestamp>(this, "birthDay");
        }

        /**
         * dirのプロパティ名を返します。
         *
         * @return dirのプロパティ名
         */
        public PropertyName<String> dir() {
            return new PropertyName<String>(this, "dir");
        }

        /**
         * templateのプロパティ名を返します。
         *
         * @return templateのプロパティ名
         */
        public PropertyName<String> template() {
            return new PropertyName<String>(this, "template");
        }

        /**
         * userListのプロパティ名を返します。
         * 
         * @return userListのプロパティ名
         */
        public _UserNames userList() {
            return new _UserNames(this, "userList");
        }
    }
}
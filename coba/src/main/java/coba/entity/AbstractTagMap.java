package coba.entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class AbstractTagMap extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    /** idプロパティ */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(precision = 19, nullable = false, unique = true)
    public Long id;

    /** clientIdプロパティ */
    @Column(precision = 19, nullable = false, unique = false)
    public Long clientId;

    /** tagIdプロパティ */
    @Column(precision = 19, nullable = false, unique = false)
    public Long tagId;

    /** productIdプロパティ */
    @Column(precision = 19, nullable = false, unique = false)
    public Long productId;

    /** tag関連プロパティ */
    @ManyToOne
    public AbstractTags tag;

    /** product関連プロパティ */
    @ManyToOne
    public AbstractProducts product;
}
package coba.entity;

import coba.entity.ProductsNames._ProductsNames;
import coba.entity.TagsNames._TagsNames;

import javax.annotation.Generated;
import org.seasar.extension.jdbc.name.PropertyName;

/**
 * {@link AbstractTagMap}のプロパティ名の集合です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.45", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "Aug 26, 2013 4:41:07 PM")
public class TagMapNames {

    /**
     * idのプロパティ名を返します。
     * 
     * @return idのプロパティ名
     */
    public static PropertyName<Long> id() {
        return new PropertyName<Long>("id");
    }

    /**
     * clientIdのプロパティ名を返します。
     * 
     * @return clientIdのプロパティ名
     */
    public static PropertyName<Long> clientId() {
        return new PropertyName<Long>("clientId");
    }

    /**
     * tagIdのプロパティ名を返します。
     * 
     * @return tagIdのプロパティ名
     */
    public static PropertyName<Long> tagId() {
        return new PropertyName<Long>("tagId");
    }

    /**
     * productIdのプロパティ名を返します。
     * 
     * @return productIdのプロパティ名
     */
    public static PropertyName<Long> productId() {
        return new PropertyName<Long>("productId");
    }

    /**
     * tagのプロパティ名を返します。
     * 
     * @return tagのプロパティ名
     */
    public static _TagsNames tag() {
        return new _TagsNames("tag");
    }

    /**
     * productのプロパティ名を返します。
     * 
     * @return productのプロパティ名
     */
    public static _ProductsNames product() {
        return new _ProductsNames("product");
    }

    /**
     * @author S2JDBC-Gen
     */
    public static class _TagMapNames extends PropertyName<AbstractTagMap> {

        /**
         * インスタンスを構築します。
         */
        public _TagMapNames() {
        }

        /**
         * インスタンスを構築します。
         * 
         * @param name
         *            名前
         */
        public _TagMapNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         * 
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _TagMapNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * idのプロパティ名を返します。
         *
         * @return idのプロパティ名
         */
        public PropertyName<Long> id() {
            return new PropertyName<Long>(this, "id");
        }

        /**
         * clientIdのプロパティ名を返します。
         *
         * @return clientIdのプロパティ名
         */
        public PropertyName<Long> clientId() {
            return new PropertyName<Long>(this, "clientId");
        }

        /**
         * tagIdのプロパティ名を返します。
         *
         * @return tagIdのプロパティ名
         */
        public PropertyName<Long> tagId() {
            return new PropertyName<Long>(this, "tagId");
        }

        /**
         * productIdのプロパティ名を返します。
         *
         * @return productIdのプロパティ名
         */
        public PropertyName<Long> productId() {
            return new PropertyName<Long>(this, "productId");
        }

        /**
         * tagのプロパティ名を返します。
         * 
         * @return tagのプロパティ名
         */
        public _TagsNames tag() {
            return new _TagsNames(this, "tag");
        }

        /**
         * productのプロパティ名を返します。
         * 
         * @return productのプロパティ名
         */
        public _ProductsNames product() {
            return new _ProductsNames(this, "product");
        }
    }
}
package coba.entity;

import coba.entity.ClientNames._ClientNames;

import javax.annotation.Generated;
import org.seasar.extension.jdbc.name.PropertyName;

/**
 * {@link AbstractUser}のプロパティ名の集合です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.45", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "Aug 26, 2013 4:41:07 PM")
public class UserNames {

    /**
     * idのプロパティ名を返します。
     * 
     * @return idのプロパティ名
     */
    public static PropertyName<Long> id() {
        return new PropertyName<Long>("id");
    }

    /**
     * displayNameのプロパティ名を返します。
     * 
     * @return displayNameのプロパティ名
     */
    public static PropertyName<String> displayName() {
        return new PropertyName<String>("displayName");
    }

    /**
     * loginIdのプロパティ名を返します。
     * 
     * @return loginIdのプロパティ名
     */
    public static PropertyName<String> loginId() {
        return new PropertyName<String>("loginId");
    }

    /**
     * passwordのプロパティ名を返します。
     * 
     * @return passwordのプロパティ名
     */
    public static PropertyName<String> password() {
        return new PropertyName<String>("password");
    }

    /**
     * statusのプロパティ名を返します。
     * 
     * @return statusのプロパティ名
     */
    public static PropertyName<byte[]> status() {
        return new PropertyName<byte[]>("status");
    }

    /**
     * clientIdのプロパティ名を返します。
     * 
     * @return clientIdのプロパティ名
     */
    public static PropertyName<Long> clientId() {
        return new PropertyName<Long>("clientId");
    }

    /**
     * clientのプロパティ名を返します。
     * 
     * @return clientのプロパティ名
     */
    public static _ClientNames client() {
        return new _ClientNames("client");
    }

    /**
     * @author S2JDBC-Gen
     */
    public static class _UserNames extends PropertyName<AbstractUser> {

        /**
         * インスタンスを構築します。
         */
        public _UserNames() {
        }

        /**
         * インスタンスを構築します。
         * 
         * @param name
         *            名前
         */
        public _UserNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         * 
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _UserNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * idのプロパティ名を返します。
         *
         * @return idのプロパティ名
         */
        public PropertyName<Long> id() {
            return new PropertyName<Long>(this, "id");
        }

        /**
         * displayNameのプロパティ名を返します。
         *
         * @return displayNameのプロパティ名
         */
        public PropertyName<String> displayName() {
            return new PropertyName<String>(this, "displayName");
        }

        /**
         * loginIdのプロパティ名を返します。
         *
         * @return loginIdのプロパティ名
         */
        public PropertyName<String> loginId() {
            return new PropertyName<String>(this, "loginId");
        }

        /**
         * passwordのプロパティ名を返します。
         *
         * @return passwordのプロパティ名
         */
        public PropertyName<String> password() {
            return new PropertyName<String>(this, "password");
        }

        /**
         * statusのプロパティ名を返します。
         *
         * @return statusのプロパティ名
         */
        public PropertyName<byte[]> status() {
            return new PropertyName<byte[]>(this, "status");
        }

        /**
         * clientIdのプロパティ名を返します。
         *
         * @return clientIdのプロパティ名
         */
        public PropertyName<Long> clientId() {
            return new PropertyName<Long>(this, "clientId");
        }

        /**
         * clientのプロパティ名を返します。
         * 
         * @return clientのプロパティ名
         */
        public _ClientNames client() {
            return new _ClientNames(this, "client");
        }
    }
}
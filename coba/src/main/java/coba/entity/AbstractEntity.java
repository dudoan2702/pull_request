package coba.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.annotation.Resource;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import coba.Config;

@MappedSuperclass
public abstract class AbstractEntity implements Serializable {
    @Resource
    protected Config config;

    private static final long serialVersionUID = 1L;
    @Column(nullable = false, unique = false)
    public boolean deleted;
    @Column(nullable = true, unique = false)
    public Long updatedBy;
    @Column(nullable = true, unique = false, columnDefinition="datetime")
    public Timestamp updatedAt;
    @Column(nullable = true, unique = false)
    public Long createdBy;
    @Column(nullable = true, unique = false, columnDefinition="datetime")
    public Timestamp createdAt;

    public void setParamsForNew(Long creatorUserId){
        Timestamp now = new Timestamp(System.currentTimeMillis());
        this.deleted = false;
        this.createdAt = now;
        this.createdBy = creatorUserId;
        this.setParamsForUpdate(creatorUserId);
    }

    public void setParamsForUpdate(Long updaterUserId){
        Timestamp now = new Timestamp(System.currentTimeMillis());
        this.updatedAt = now;
        this.updatedBy = updaterUserId;
    }

    public void setParamsForDelete(Long deleterUserId){
        this.deleted = true;
        this.setParamsForUpdate(deleterUserId);
    }
}
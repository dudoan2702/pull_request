package coba.entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class AbstractUser extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    /** idプロパティ */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(precision = 19, nullable = false, unique = true)
    public Long id;

    /** displayNameプロパティ */
    @Column(length = 100, nullable = false, unique = false)
    public String displayName;

    /** loginIdプロパティ */
    @Column(length = 30, nullable = false, unique = false)
    public String loginId;

    /** passwordプロパティ */
    @Column(length = 100, nullable = false, unique = false)
    public String password;

    /** statusプロパティ */
    @Column(length = 2, nullable = false, unique = false)
    public byte[] status;

    /** clientIdプロパティ */
    @Column(precision = 19, nullable = false, unique = false)
    public Long clientId;
/*
    *//** client関連プロパティ *//*
    @ManyToOne
    public AbstractClient client;*/
}
package coba.dao;

import coba.entity.AbstractTags;
import java.util.List;
import javax.annotation.Generated;

import static coba.entity.TagsNames.*;
import static org.seasar.extension.jdbc.operation.Operations.*;

/**
 * {@link AbstractTags}のサービスクラスです。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.45", "org.seasar.extension.jdbc.gen.internal.model.ServiceModelFactoryImpl"}, date = "Aug 26, 2013 4:37:36 PM")
public class TagsDao extends AbstractDao<AbstractTags> {

    /**
     * 識別子でエンティティを検索します。
     * 
     * @param id
     *            識別子
     * @return エンティティ
     */
    public AbstractTags findById(Long id) {
        return select().id(id).getSingleResult();
    }

    /**
     * 識別子の昇順ですべてのエンティティを検索します。
     * 
     * @return エンティティのリスト
     */
    public List<AbstractTags> findAllOrderById() {
        return select().orderBy(asc(id())).getResultList();
    }
}
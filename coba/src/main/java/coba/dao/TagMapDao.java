package coba.dao;

import static org.seasar.extension.jdbc.operation.Operations.asc;

import java.util.List;

import javax.annotation.Generated;

import coba.entity.AbstractTagMap;
import coba.entity.TagMapNames;

@Generated(value = {"S2JDBC-Gen 2.4.45", "org.seasar.extension.jdbc.gen.internal.model.ServiceModelFactoryImpl"}, date = "Aug 26, 2013 4:37:36 PM")
public class TagMapDao extends AbstractDao<AbstractTagMap> {

    public AbstractTagMap findById(Long id) {
        return select().id(id).getSingleResult();
    }

    public List<AbstractTagMap> findAllOrderById() {
        return select().orderBy(asc(TagMapNames.id())).getResultList();
    }
}
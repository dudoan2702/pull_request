package coba.dao;

import java.util.List;

import coba.entity.AbstractClient;

public class ClientDao extends AbstractDao<AbstractClient> {
    public AbstractClient findById(Long id) {
        return select().id(id).getSingleResult();
    }
    public List<AbstractClient> findAllOrderById() {
        return select().getResultList();
    }
}
package coba.dao;

import javax.annotation.Resource;

import org.seasar.extension.jdbc.service.S2AbstractService;

import coba.Config;

public abstract class AbstractDao<ENTITY> extends S2AbstractService<ENTITY> {
    @Resource
    protected Config config;
}
package coba.dao;

import static org.seasar.extension.jdbc.operation.Operations.asc;
import java.util.List;
import coba.entity.User;
import coba.entity.UserNames;

public class UserDao extends AbstractDao<User> {

    public User test(Long id){
        return select().id(id).getSingleResult();
    }

    public List<User> findAllOrderById() {
        return select().orderBy(asc(UserNames.id())).getResultList();
    }
}
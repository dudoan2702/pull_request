package coba.dao;

import static coba.entity.ProductsNames.id;
import static org.seasar.extension.jdbc.operation.Operations.asc;

import java.util.List;

import coba.entity.AbstractProducts;

public class ProductsDao extends AbstractDao<AbstractProducts> {
    public AbstractProducts findById(Long id) {
        return select().id(id).getSingleResult();
    }
    public List<AbstractProducts> findAllOrderById() {
        return select().orderBy(asc(id())).getResultList();
    }
}
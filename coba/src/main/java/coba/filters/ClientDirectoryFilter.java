package coba.filters;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.seasar.framework.container.S2Container;
import org.seasar.framework.container.factory.SingletonS2ContainerFactory;
import org.seasar.framework.util.StringUtil;
import org.seasar.struts.config.S2ExecuteConfig;
import org.seasar.struts.filter.RoutingFilter;
import org.seasar.struts.util.RequestUtil;
import org.seasar.struts.util.RoutingUtil;
import org.seasar.struts.util.S2ExecuteConfigUtil;
import org.seasar.struts.util.URLEncoderUtil;

public class ClientDirectoryFilter extends RoutingFilter {

    @Override
    public void init(FilterConfig config) throws ServletException {
        super.init(config);
    }
    @Override
    public void destroy() {
        super.destroy();
    }
    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        String contextPath = req.getContextPath();
        //アプリケーションのルートの設定
        // スラッシュ一本で来たらリセット
        if (contextPath.equals("/")) {
            contextPath = "";
        }
        // ドメインからのパスを取得
        String path = RequestUtil.getPath(req);
        //jsp直接的なアクセスをするならtomcatにそのまま流す
        if (!processDirectAccess(request, response, chain, path)) {
            return;
        }
        //ディレクトリ部分を除去
        // pathは /hoge/piyo/fuge/moge
        // hogeディレクトリ、piyoデバイス、fugaアクション、mogeメソッド
        // というものを想定
        //ディレクトリを除去
        String simplePath = path.replaceFirst("^/.+?/", "/");
        //ディレクトリを取り出す
        String dir = path.split("/")[1];
        if (path.indexOf('.') < 0) {
            //パス上に「.」が存在しない・・・拡張子指定系のファイルアクセスっぽくないやつかな？

            //パスの階層を分解
            String[] names = StringUtil.split(simplePath, "/");
            //DIされているものを取得する準備
            S2Container container = SingletonS2ContainerFactory.getContainer();
            StringBuilder sb = new StringBuilder(50);
            //分解したパスで順繰りループ
            for (int i = 0; i < names.length; i++) {
                //指定されたパスのアクションが存在するか否かチェック
                if (container.hasComponentDef(sb + names[i] + "Action")) {
                    //存在する！
                    //どの階層までがアクション名でどの階層からがパラメータなのかを指定してそれぞれ取り出す
                    String actionPath = RoutingUtil.getActionPath(names, i);
                    String paramPath = RoutingUtil.getParamPath(names, i + 1);

                    if (StringUtil.isEmpty(paramPath)) {
                        if (!path.endsWith("/")) {
                            //アクションしか指定が無くパラメータが無くケツがスラッシュで終わっていない
                            //→アクションに続きケツにスラッシュをつけてクエリをズラズラそのまま連結
                            //スラッシュ付きでアクセスし直させる（リダイレクト）
                            //そのままリダイレクトさせるだけなので pathはオリジナルを使いクエリも加工しない
                            String queryString = "";
                            if (req.getQueryString() != null) {
                                queryString = "?" + req.getQueryString();
                            }
                            //リクエストパラメータ（？でズラズラくっつけるやつ）
                            res.sendRedirect(contextPath + path + "/" + queryString);
                            return;
                        } else if (S2ExecuteConfigUtil.findExecuteConfig(actionPath, req) != null) {
                            //アクションしか指定が無くパラメータが無くケツがスラッシュで終わっている（完全にパラメータが無い）
                            //プレーンにフロントコントローラーに受け渡し
                            forward((HttpServletRequest) request,
                                    (HttpServletResponse) response, actionPath,
                                    null, null, dir);
                            return;
                        }
                    } else {
                        //actionより先があるやつ。メソッド指定があるかどうか確認するよ
                        S2ExecuteConfig executeConfig = S2ExecuteConfigUtil
                                .findExecuteConfig(actionPath, paramPath);
                        if (executeConfig != null) {
                            //メソッド指定がある
                            forward((HttpServletRequest) request,
                                    (HttpServletResponse) response, actionPath,
                                    paramPath, executeConfig, dir);
                            return;
                        }
                    }
                }
                //ここ以下はActionが存在しなかった場合よ・・・つまりディレクトリ掘ってる or index
                if (container.hasComponentDef(sb + "indexAction")) {
                    //indexをactionとして↑と同じように処理
                    String actionPath = RoutingUtil.getActionPath(names, i - 1)
                            + "/index";
                    String paramPath = RoutingUtil.getParamPath(names, i);
                    if (StringUtil.isEmpty(paramPath)) {
                        if (!path.endsWith("/")) {
                            //スラッシュの付け替えのためにリダイレクトするだけなので
                            //dirの考慮はせずにそのままスルー
                            String queryString = "";
                            if (req.getQueryString() != null) {
                                queryString = "?" + req.getQueryString();
                            }
                            res.sendRedirect(contextPath + path + "/" + queryString);
                            return;
                        } else if (S2ExecuteConfigUtil.findExecuteConfig(
                                actionPath, req) != null) {
                            forward((HttpServletRequest) request,
                                    (HttpServletResponse) response, actionPath,
                                    null, null, dir);
                            return;
                        }
                    } else {
                        S2ExecuteConfig executeConfig = S2ExecuteConfigUtil
                                .findExecuteConfig(actionPath, paramPath);
                        if (executeConfig != null) {
                            forward((HttpServletRequest) request,
                                    (HttpServletResponse) response, actionPath,
                                    paramPath, executeConfig, dir);
                            return;
                        }
                    }
                }
                //ここ以下はActionが存在しなかった場合よ・・・つまりディレクトリ掘ってる
                //ディレクトリ階層をアンスコで表現し↑と同じしょりを続ける
                sb.append(names[i] + "_");
            }
            //最後までいってもactionも何も無い場合
            if (container.hasComponentDef(sb + "indexAction")) {
                //最後の最後のindex処理をやる
                String actionPath = RoutingUtil.getActionPath(names,
                        names.length - 1)
                        + "/index";
                if (!path.endsWith("/")) {
                    String queryString = "";
                    if (req.getQueryString() != null) {
                        queryString = "?" + req.getQueryString();
                    }
                    res.sendRedirect(contextPath + path + "/" + queryString);
                    return;
                } else if (S2ExecuteConfigUtil.findExecuteConfig(actionPath,
                        req) != null) {
                    forward((HttpServletRequest) request,
                            (HttpServletResponse) response, actionPath, null,
                            null, dir);
                    return;
                }
            }
            //最後の最後の最後まで何もなかったら↓知らんから別のフィルターにぶん投げる
        }
        chain.doFilter(request, response);
    }

    @Override
    protected boolean processDirectAccess(ServletRequest request,
            ServletResponse response, FilterChain chain, String path)
            throws IOException {
        return super.processDirectAccess(request, response, chain, path);
    }

    protected void forward(HttpServletRequest request,
            HttpServletResponse response, String actionPath, String paramPath,
            S2ExecuteConfig executeConfig, String dir) throws IOException, ServletException {


        String forwardPath = actionPath + ".do";
        if (executeConfig != null) {
            forwardPath = forwardPath + executeConfig.getQueryString(paramPath) + "&dir=" + dir;
        }else{
//            forwardPath = forwardPath + "&dir=" + dir;
            forwardPath = forwardPath + "?dir=" + URLEncoderUtil.encode(dir);
        }
        request.getRequestDispatcher(forwardPath).forward(request, response);
    }


}

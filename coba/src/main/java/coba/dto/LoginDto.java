package coba.dto;

import java.io.Serializable;
import org.seasar.framework.container.annotation.tiger.Component;
import org.seasar.framework.container.annotation.tiger.InstanceType;

import coba.entity.AbstractClient;
import coba.entity.Client;

@Component(instance=InstanceType.SESSION)
public class LoginDto implements Serializable{
	private static final long serialVersionUID = 1L;
	public AbstractClient client = null;
	public AbstractClient getClient(){
	    client = new Client();
	    return client;
	}
}
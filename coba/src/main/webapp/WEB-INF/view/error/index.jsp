<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<tiles:insert template="/WEB-INF/view/common/layout.jsp" flush="true">
    <c:set var="contextPath" value="${pageContext.request.contextPath}" />
    <tiles:put name="style" value="rankSet/index" />
    <tiles:put name="title" value="エラーページ" />
    <tiles:put name="content" type="string">
        <div id="content-main">
            <div id="content-main1">
                <div class="head-title">
                    <span class="title">エラーページ</span>
                </div>
                <div id="errors" style="display: block;">
                    <div>
                        <ul class="ul-error">
                            <li>不正なアクセスです。</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </tiles:put>
</tiles:insert>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <title><tiles:getAsString name="title" /></title>
    <c:set var="contextPath" value="${ pageContext.request.contextPath }"/>
    <c:set var="style"><tiles:getAsString name="style" ignore="true" /></c:set>
    <c:if test="${ !empty(style) }"><link rel="stylesheet" href="${contextPath}/css/${style}.css" /></c:if>
</head>
<body>
    <tiles:insert attribute="content1" ignore="true" />
    <tiles:insert attribute="content2" ignore="true" />
    <tiles:insert attribute="content3" ignore="true" />
    <tiles:insert attribute="content4" ignore="true" />
    <tiles:insert attribute="content5" ignore="true" />
    <tiles:insert attribute="content6" ignore="true" />
    <tiles:insert attribute="content7" ignore="true" />
    <tiles:insert attribute="content8" ignore="true" />
    <tiles:insert attribute="content9" ignore="true" />
    <tiles:insert attribute="content10" ignore="true" />

    <%--Add script here to boost loading speed--%>
    <c:set var="contextPath" value="${ pageContext.request.contextPath }"/>
    <script src="${contextPath}/js/jquery-2.0.3.js"></script>
    <c:set var="js1"><tiles:getAsString name="js1" ignore="true" /></c:set>
    <c:set var="js2"><tiles:getAsString name="js2" ignore="true" /></c:set>
    <c:set var="js3"><tiles:getAsString name="js3" ignore="true" /></c:set>
    <c:set var="js4"><tiles:getAsString name="js4" ignore="true" /></c:set>
    <c:set var="js5"><tiles:getAsString name="js5" ignore="true" /></c:set>
    <c:if test="${ !empty(js1) }"><script src="${contextPath}/js/${js1}.js"></script></c:if>
    <c:if test="${ !empty(js2) }"><script src="${contextPath}/js/${js2}.js"></script></c:if>
    <c:if test="${ !empty(js3) }"><script src="${contextPath}/js/${js3}.js"></script></c:if>
    <c:if test="${ !empty(js4) }"><script src="${contextPath}/js/${js4}.js"></script></c:if>
    <c:if test="${ !empty(js5) }"><script src="${contextPath}/js/${js5}.js"></script></c:if>
    <%--End script--%>
</body>
</html>